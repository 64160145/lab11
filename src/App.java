public class App {
    public static void main(String[] args) throws Exception {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.Swim();
        man1.walk();
        man1.run();
        Bat bat1 = new Bat("Bat");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        Snake snake1 = new Snake("Snake");
        snake1.craw();
        snake1.eat();
        snake1.sleep();
        Rat rat1 = new Rat("rat1");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        Dog dog1 = new Dog("Dog1");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.Swim();
        Cat cat1 = new Cat("cat1");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        cat1.Swim();
        Crocodile crocodile1 = new Crocodile("Crocodile1");
        crocodile1.craw();
        crocodile1.eat();
        crocodile1.sleep();
        Fish fish1 = new Fish("Fish1");
        fish1.Swim();
        fish1.eat();
        fish1.sleep();

        Flyable[] flyables = {bird1,boeing,clark,bat1};
        for(int i = 0; i < flyables.length;i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        Walkable[] walkables = {bird1,clark,man1,rat1,dog1,cat1};
        for(int i = 0; i < walkables.length;i++){
            walkables[i].walk();
            walkables[i].run();
        }
        Crawable[] crawables = {snake1,crocodile1};
        for(int i = 0; i < crawables.length;i++){
            crawables[i].craw();
        }
        Swimable[] swimables = {crocodile1,man1,clark,dog1,cat1,fish1};
        for(int i = 0; i < swimables.length;i++){
            swimables[i].Swim();
        }
    }
}
