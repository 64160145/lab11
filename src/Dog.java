public class Dog extends Animal implements Walkable,Swimable {
    public Dog(String name) {
        super(name, 2);
    }
    @Override
    public void sleep() {
        System.out.println(this.toString()+"sleep...");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+"eat...");
    }
    @Override
    public String toString() {
        return "Dog("+this.getName()+")";
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+"walk...");
    }

    @Override
    public void run() {
        System.out.println(this.toString()+"run...");
    }
    @Override
    public void Swim() {
        System.out.println(this.toString()+"Swim...");
    }
}
