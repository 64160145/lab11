public class Rat extends Animal implements Walkable {

    public Rat(String name) {
        super(name, 2);
    }
    @Override
    public void sleep() {
        System.out.println(this.toString()+"sleep...");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+"eat...");
    }
    @Override
    public String toString() {
        return "Rat("+this.getName()+")";
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+"walk...");
    }

    @Override
    public void run() {
        System.out.println(this.toString()+"run...");
    }
}
