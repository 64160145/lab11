public abstract class Animal {
    private String name;
    private int numberOflegs;
    public Animal(String name,int numberOflegs){
        this.name = name;
        this.numberOflegs = numberOflegs;
    }
    public String getName(){
        return this.name;
    }
    public int getNumberOflegs(){
        return this.numberOflegs;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setNumberOflegs(int numberOflegs){
        this.numberOflegs = numberOflegs;
    }
    @Override
    public String toString() {
        return "Animal("+name+")";
    }
    public abstract void sleep();
    public abstract void eat();
}
