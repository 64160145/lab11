public class Fish extends Animal implements Swimable {

    public Fish(String name) {
        super(name, 0);
    }
    @Override
    public void Swim() {
        System.out.println(this.toString()+"Swim...");
    }
    @Override
    public void sleep() {
        System.out.println(this.toString()+"sleep...");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+"eat...");
    }
}
