public class Snake extends Animal implements Crawable {

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void craw() {
        System.out.println(this.toString()+"craw...");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+"sleep...");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+"eat...");
    }
    @Override
    public String toString() {
        return "Snake("+this.getName()+")";
    }
    
}
