public class Bat extends Animal implements Flyable{
    public Bat(String name) {
        super(name, 2);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+"sleep...");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+"eat...");
    }
    @Override
    public String toString() {
        return "Bat("+this.getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString()+"takeoff...");
    }

    @Override
    public void fly() {
        System.out.println(this.toString()+"fly...");
    }

    @Override
    public void landing() {
        System.out.println(this.toString()+"landing...");    
    }
}
