public class Plane extends Vehicle implements Flyable{

    public Plane(String name, String engine) {
        super(name, engine);
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString()+"takeoff...");
    }

    @Override
    public void fly() {
        System.out.println(this.toString()+"fly...");
    }

    @Override
    public void landing() {
        System.out.println(this.toString()+"landing...");    
    }
    @Override
    public String toString() {
        return "Plane("+getName()+")";
    }
}
